// FETCH

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data)
})

// 4. Stretch goals

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	let list = data.map((todo_item) => {
		return todo_item.title
	})
	console.log(list)
})

// let arrayData = [];

// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((data) => {
// 	data.map((item)=> {
// 		arrayData.push(item.title)

// 	})
// })
// console.log(arrayData)

// SINGLE

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
	console.log(data)
})

// POST

fetch('https://jsonplaceholder.typicode.com/todos',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
    	userId: 1, 
    	id: 1, 
    	title: 'New Post', 
    	completed: true
    })
})
.then((response) => response.json())
.then((data) => console.log(data))

// 6.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then ((response) => response.json())
.then ((data) => console.log(data.title + " , " + data.completed))

// PATCH 1

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Corrected Post'
    })
})

.then((response) => response.json())
.then((data) => console.log(data))


// PATCH 2
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
    	userId: 205,
        title: 'Updated title',
        description: 'this is title',
        status: 'Completed',
        dateCompleted: '10-03-22'

    })
})

.then((response) => response.json())
.then((data) => console.log(data))


// DELETE
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'DELETE'
})

